import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Provider } from 'react-redux'
import Screen1 from './src/screen/Screen1';
import store from './src/store';

export default class App extends Component{
  render(){
    return(
      <Provider store={store}>
        <Screen1/>
      </Provider>     
    )
  }
}