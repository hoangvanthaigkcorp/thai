import {LOAD_API_GRAMMAR} from "./type"

export const loadAPIGrammar = (data)=>({ type: LOAD_API_GRAMMAR, data })