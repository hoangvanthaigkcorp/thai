import {combineReducers} from "redux"
import LoadAPIGrammar from "./LoadAPIGrammar"

export default combineReducers({
    LoadAPIGrammar,
});