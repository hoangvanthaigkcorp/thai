import React from 'react';
import {
    Text, ImageBackground, StatusBar, View, Image, Dimensions, StyleSheet
} from 'react-native';
import { Picker } from '@react-native-community/picker'
import { ScrollView, TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import LinearGradient from "react-native-linear-gradient";

var Width = Dimensions.get("window").width;
var Height = Dimensions.get('window').height;
StatusBar.setHidden(false, 'slide');
export default function HomeScreen({ navigation }, props) {
    const [search, updateSearch] = React.useState('');
    const [selectedValue, setSelectedValue] = React.useState('Chọn Danh mục');
    const [selectedCT, setCT] = React.useState('Chọn chương trình');
    return (
        <View >
            <ImageBackground
                style={{ width: Width, height: Height, resizeMode: 'contain' }}
                source={require('../../../image/nen1.png')}
            >
                <View style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 20,
                }}>
                    <View
                        style={{
                            flexDirection: "row",
                            marginTop:5
                        }}
                    >
                        <TouchableOpacity
                            onPress={() => {
                                navigation.openDrawer();
                            }}
                        >
                            {/* <View style={{ width: 100, height: 30, marginLeft: 50, marginRight: -80, marginTop: 20, backgroundColor: 'white' }}> */}
                            <Image source={require('../../../image/setting.png')}
                                style={{ width: 40, height: 30, marginLeft: 70, marginRight: 0, marginTop: 20 }}
                            />
                            {/* </View> */}
                        </TouchableOpacity >
                        <View style={{
                            backgroundColor: 'white',
                            borderRadius: 20,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderWidth: .5,
                            borderColor: '#000',
                            height: 50,
                            borderRadius: 15,
                            marginTop: 15,
                            marginLeft: 20,
                            marginRight: 50,
                            marginBottom: 0,
                        }}>
                            <TextInput style={{ fontSize: 18, color: 'black', width: 250 }} placeholder='Tìm kiếm khóa học' placeholderTextColor='gray'></TextInput>
                            <Image source={require('../../../image/search.png')} style={{ marginRight: 10, width: 20, height: 20 }} />
                        </View>
                    </View>
                    <View style={{ flex: 1, marginTop: 50 }}>
                        <ScrollView style={styles.scrollView, { width: Width, marginLeft: 80, marginBottom: 120 }}>
                            <View style={{ backgroundColor: '#fff', width: 320, height: 150, marginTop: 0, borderRadius: 10 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ flex: 0.4, textAlign: 'center', margin: 10 }}>Tổng thời gian học yêu cầu</Text>
                                    <Text style={{ flex: 0.4, textAlign: 'center', margin: 10 }}>Thời gian học của bạn</Text>
                                    <Text style={{ flex: 0.3, textAlign: 'center', margin: 10 }}>Trạng thái chung</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ flex: 0.4, textAlign: 'center', margin: 10, fontSize: 35 }}>0:00</Text>
                                    <Text style={{ flex: 0.4, textAlign: 'center', margin: 10, fontSize: 35 }}>02:20</Text>
                                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#402cc3', '#04c2e5']} style={{ paddingHorizontal: 3, borderRadius: 8, justifyContent: 'center', paddingVertical: 8, flex: 0.3, height: 30, marginTop: 20, marginRight: 10, marginLeft: 10 }}>
                                        <Text style={{ textAlign: 'center', color: 'white' }}>Đang học</Text>
                                    </LinearGradient>
                                </View>
                            </View>
                            <Text style={{ fontWeight: 'bold', marginTop: 10 }}>Danh sách chương trình</Text>
                            <View style={{ backgroundColor: '#fff', width: 230, height: 100, alignItems: 'center', marginTop: 15, borderRadius: 10, marginLeft: 50 }}>
                                <View style={{ flexDirection: 'column' }}>
                                    <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center', borderRadius: 10, borderWidth: 1, borderColor: '#04c2e5', width: 180, height: 30, marginBottom: 10 }}>
                                        <Picker
                                            selectedValue={selectedValue}
                                            style={{ height: 50, width: 150, color: 'green' }}
                                            mode={"dropdown"}
                                            onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)
                                            }
                                        >
                                            <Picker.Item label="Sách" value="Sách" />
                                            <Picker.Item label="Vở" value="Vở" />
                                        </Picker>
                                    </View>
                                    <View style={styles.container, { marginTop: 10, justifyContent: 'center', alignItems: 'center', borderRadius: 10, borderWidth: 1, borderColor: '#04c2e5', width: 180, height: 30, marginBottom: 10 }}>
                                        <Picker
                                            selectedValue={selectedCT}
                                            style={{ height: 50, width: 150, color: 'green' }}
                                            mode={"dropdown"}
                                            onValueChange={(itemValue, itemIndex) => setCT(itemValue)
                                            }
                                        >
                                            <Picker.Item label="Sách" value="Sách" />
                                            <Picker.Item label="Vở" value="Vở" />
                                        </Picker>
                                    </View>
                                </View>
                            </View>
                            <Text style={{ fontWeight: 'bold', marginTop: 10 }}>4 khóa học</Text>
                            <View style={{ marginBottom: 10, flex: 1, flexDirection: 'row', marginRight: 70, backgroundColor: 'white', borderRadius: 10, width: 320, height: 160 }}>
                                <Image source={require('../../../image/tay.png')} style={{ width: 320, height: 162, resizeMode: 'stretch', borderRadius: 10 }} />
                                <Text style={{ marginLeft: -215, width: 220, fontWeight: 'bold', fontSize: 14, marginTop: 5 }}>Tổng quan về Chương trình đào tạo cơ bản và cơ chế đánh giá</Text>
                                <Text style={{ marginLeft: -220, width: 220, fontSize: 11, marginTop: 50 }}>Đã học:                       Thời lượng:30'</Text>
                                <Text style={{ marginLeft: -220, width: 220, fontSize: 11, marginTop: 70 }}>Truy cập lần cuối:      18/05/2020 14:31:03</Text>
                                <View style={{ marginLeft: -180, width: 120, fontSize: 11, marginTop: 100, height: 100 }}>
                                    <TouchableOpacity>
                                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#402cc3', '#04c2e5']} style={{ paddingHorizontal: 3, borderRadius: 20, justifyContent: 'center', paddingVertical: 8, height: 30, marginTop: 20, marginRight: 0, marginLeft: 0 }}>
                                            <Text style={{ textAlign: 'center', color: 'white' }}>Hoàn thành</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ marginBottom: 10, flex: 1, flexDirection: 'row', marginRight: 70, backgroundColor: 'white', borderRadius: 10, width: 320, height: 160 }}>
                                <Image source={require('../../../image/tay.png')} style={{ width: 320, height: 162, resizeMode: 'stretch', borderRadius: 10 }} />
                                <Text style={{ marginLeft: -215, width: 220, fontWeight: 'bold', fontSize: 14, marginTop: 5 }}>Tổng quan về Chương trình đào tạo cơ bản và cơ chế đánh giá</Text>
                                <Text style={{ marginLeft: -220, width: 220, fontSize: 11, marginTop: 50 }}>Đã học:                       Thời lượng:30'</Text>
                                <Text style={{ marginLeft: -220, width: 220, fontSize: 11, marginTop: 70 }}>Truy cập lần cuối:      18/05/2020 14:31:03</Text>
                                <View style={{ marginLeft: -180, width: 120, fontSize: 11, marginTop: 100, height: 100 }}>
                                    <TouchableOpacity>
                                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#402cc3', '#04c2e5']} style={{ paddingHorizontal: 3, borderRadius: 20, justifyContent: 'center', paddingVertical: 8, height: 30, marginTop: 20, marginRight: 0, marginLeft: 0 }}>
                                            <Text style={{ textAlign: 'center', color: 'white' }}>Hoàn thành</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ marginBottom: 10, flex: 1, flexDirection: 'row', marginRight: 70, backgroundColor: 'white', borderRadius: 10, width: 320, height: 160 }}>
                                <Image source={require('../../../image/tay.png')} style={{ width: 320, height: 162, resizeMode: 'stretch', borderRadius: 10 }} />
                                <Text style={{ marginLeft: -215, width: 220, fontWeight: 'bold', fontSize: 14, marginTop: 5 }}>Tổng quan về Chương trình đào tạo cơ bản và cơ chế đánh giá</Text>
                                <Text style={{ marginLeft: -220, width: 220, fontSize: 11, marginTop: 50 }}>Đã học:                       Thời lượng:30'</Text>
                                <Text style={{ marginLeft: -220, width: 220, fontSize: 11, marginTop: 70 }}>Truy cập lần cuối:      18/05/2020 14:31:03</Text>
                                <View style={{ marginLeft: -180, width: 120, fontSize: 11, marginTop: 100, height: 100 }}>
                                    <TouchableOpacity>
                                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#402cc3', '#04c2e5']} style={{ paddingHorizontal: 3, borderRadius: 20, justifyContent: 'center', paddingVertical: 8, height: 30, marginTop: 20, marginRight: 0, marginLeft: 0 }}>
                                            <Text style={{ textAlign: 'center', color: 'white' }}>Hoàn thành</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ marginBottom: 10, flex: 1, flexDirection: 'row', marginRight: 70, backgroundColor: 'white', borderRadius: 10, width: 320, height: 160 }}>
                                <Image source={require('../../../image/tay.png')} style={{ width: 320, height: 162, resizeMode: 'stretch', borderRadius: 10 }} />
                                <Text style={{ marginLeft: -215, width: 220, fontWeight: 'bold', fontSize: 14, marginTop: 5 }}>Tổng quan về Chương trình đào tạo cơ bản và cơ chế đánh giá</Text>
                                <Text style={{ marginLeft: -220, width: 220, fontSize: 11, marginTop: 50 }}>Đã học:                       Thời lượng:30'</Text>
                                <Text style={{ marginLeft: -220, width: 220, fontSize: 11, marginTop: 70 }}>Truy cập lần cuối:      18/05/2020 14:31:03</Text>
                                <View style={{ marginLeft: -180, width: 120, fontSize: 11, marginTop: 100, height: 100 }}>
                                    <TouchableOpacity>
                                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#402cc3', '#04c2e5']} style={{ paddingHorizontal: 3, borderRadius: 20, justifyContent: 'center', paddingVertical: 8, height: 30, marginTop: 20, marginRight: 0, marginLeft: 0 }}>
                                            <Text style={{ textAlign: 'center', color: 'white' }}>Hoàn thành</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <View style={styles.floatingButtonStyle}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                    // style={styles.touchableOpacityStyle}
                    >
                        <Image
                            style={{
                                resizeMode: 'contain',
                                width: 70,
                                height: 70
                            }}
                            source={require('../../../image/button.png')}
                        />
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00D0BF',
        borderWidth: .5,
        borderColor: '#000',
        height: 40,
        borderRadius: 5,
        marginTop: 30,
        marginLeft: 50,
        marginRight: 50,
        marginBottom: 0,
    },
    floatingButtonStyle: {
        position: 'absolute',
        bottom: 100,
        right: 0,
        marginRight:10
        //backgroundColor:'black'
    },
    scrollView: {
    },
});