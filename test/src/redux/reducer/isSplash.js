import Types from '../types';

const initialState = {
    isSplash: true
  };

const isSplash = (state = initialState, action) => {
    const { type, param } = action;
    switch (type) {
      // Change og code
      case Types.SET_SPLASH: {
        return {
          // State
          ...state,
          // Redux Store
          isSplash: param,
        }
      }
  
      // Default
      default: {
        return state;
      }
    }
};

export default isSplash;
