import Types from '../types';

const initialState = {
    show_hotline: false
  };

const showHotLine = (state = initialState, action) => {
    const { type, param } = action;
    switch (type) {
      // Change og code
      case Types.SHOW_HOTLINE: {
        return {
          // State
          ...state,
          // Redux Store
          show_hotline: param,
        }
      }

      // Default
      default: {
        return state;
      }
    }
};

export default showHotLine;

