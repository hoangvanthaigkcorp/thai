import {combineReducers} from 'redux';
import infoUser from './userReducer';
import ogData from './setOG_Data';
import isSplash from './isSplash';
import hotline from './showHotline';

const rootReducer = combineReducers({
    infoUser, ogData, isSplash, hotline
});

export default rootReducer;
