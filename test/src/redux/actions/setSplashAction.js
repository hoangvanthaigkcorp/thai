import Types from '../types';
const setSplash = (param) => ({
    type: Types.SET_SPLASH,
    param
});

export {setSplash}