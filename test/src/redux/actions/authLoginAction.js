import Types from '../types';
const loginSuccess = (param) => ({
    type: Types.LOGIN_SUCCESS,
    param
});

export {loginSuccess}