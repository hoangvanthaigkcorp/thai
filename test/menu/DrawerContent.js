import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer'
import {
    Avatar,
    Title,
    Drawer,
}
from 'react-native-paper'
export  function DrawerContent(props) {
    return (
        <View style={{ flex: 1 }}>
            <DrawerContentScrollView {...props}>
                <View styl={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                            <Avatar.Image
                                source={require('../image/icon1.png')}
                                size={50}
                            />
                        </View>
                        <View style={{
                            marginLeft: 15,
                            flexDirection: 'column'
                        }}>
                            <Title style={styles.title}>Lưu Tuấn Minh
                            </Title>
                        </View>
                    </View>
                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem
                            icon={({ color, size }) => (
                                <Image
                                    source={require('../image/menu_icon1.png')}
                                    style={{ height: 40, width: 40 }}
                                    resizeMode={'contain'}
                                />
                            )}
                            label="Hồ sơ của tôi"
                            onPress={() => { }}
                        />
                        <DrawerItem
                        style={{width:300}}
                            icon={({ color, size }) => (
                                <Image
                                    source={require('../image/menu_icon2.png')}
                                    style={{ height: 40, width: 40 }}
                                    resizeMode={'contain'}
                                />
                            )}
                            label="Điều kiện xét cấp chứng chỉ"
                            onPress={() => { }}
                        />
                        <DrawerItem
                        style={{width:200}}
                            icon={({ color, size }) => (
                                <Image
                                    source={require('../image/menu_icon3.png')}
                                    style={{ height: 40, width: 40 }}
                                    resizeMode={'contain'}
                                />
                            )}
                            label="Đăng xuất"
                            onPress={() => {
                                props.navigation.navigate("LoginScreen")
                            }}
                        />
                    </Drawer.Section>
                </View>
            </DrawerContentScrollView>
        </View>
    );
}
const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3,
    },
    drawerSection: {
        marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1,
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
})