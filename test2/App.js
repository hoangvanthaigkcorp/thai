import * as React from 'react';
import { Text, View, Button,TextInput,Image,StyleSheet,Animated,Dimensions  } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderBackground,useHeaderHeight } from '@react-navigation/stack';
import { TouchableOpacity } from 'react-native-gesture-handler';
import LottieView from 'lottie-react-native';
//const headerHeight = useHeaderHeight();
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

function HomeScreen({ navigation }) {
  const [num1, setNum1] = React.useState('');
  const [num2, setNum2] = React.useState('');
  const [width,setWidth] = React.useState(100);
  const [height,setHeight] = React.useState(100);
  
  //tinh tong
  var getNum1 = parseFloat(num1);
  var getNum2 = parseFloat(num2);
  var sum = getNum1+getNum2;
  //set chieu cao cho anh
  var getWidth = parseFloat(width);
  var getHeight = parseFloat(height);
  //tao bien dem
  const [count, setCount] = React.useState(0);
  
  
  
  //layout effect
  React.useLayoutEffect(() => {
    console.log('layout efffect');
    //setWidth(300);
    //setHeight(300);
  });
  React.useEffect(() => {
    console.log('You clicked '+count+ ' times');
    //setWidth(200);
    //setHeight(200);
    return()=>{
      console.log(count)
    }
  });


  return (
    //justifyContent: 'center' (can giua man hinh theo chieu doc)
    //alignItems: 'center' (can giua man hinh theo chieu ngang o giua)
    <View style={{ flex: 1, alignItems: 'stretch' }}>
      <Text style={{marginBottom:20,alignItems:'center'}}>Home Screen</Text>
        <View style={{marginBottom:20,alignItems:'center'}}>
          <TouchableOpacity onPress={()=>{
            alert("Bạn đã click vào ảnh")
          }}>
            <Image
                //style={styles.tinyLogo}
                style={{width:width,height:height,maxWidth:500,maxHeight:360,minWidth:100,minHeight:100}}
                source={require('./images/test.gif')}
            />
          </TouchableOpacity>
        </View>
    <TextInput
      style={{ 
    	height: 40, 
    	borderColor: 'gray', 
      borderWidth: 1,
      marginBottom:10
    }}
      placeholderTextColor = 'black'
      onChangeText={text => setNum1(text)}
      value={num1}
      keyboardType = 'numeric'
      placeholder="Insert your text!"
      onSubmitEditing={ () => {
        if(num1!='' && num2!=''){
          navigation.navigate('Details', {
            itemId: 1,
            otherParam:sum,
          });
          setNum1('');
          setNum2('');
        }
      }}
    />
    <TextInput
      style={{ 
    	height: 40, 
    	borderColor: 'gray', 
      borderWidth: 1,
      marginBottom:10
    }}
      placeholderTextColor = 'black'
      onChangeText={text => setNum2(text)}
      value={num2}
      keyboardType = 'numeric'
      placeholder="Insert here...!"
      onSubmitEditing={ () => {
        if(num1!='' && num2!=''){
          navigation.navigate('Details', {
            itemId: 1,
            otherParam:sum,
          });
          setNum1('');
          setNum2('');
        }
      }}
    />
     <TextInput
      style={{ 
    	height: 40, 
    	borderColor: 'gray', 
      borderWidth: 1,
      marginBottom:10
    }}
      placeholderTextColor = 'black'
      onChangeText={text =>{ 
        if(text!=''){
            setWidth(parseFloat(text));            
      }
      else
        setWidth(0);
      }
    }
      value={getWidth+""}
      keyboardType = 'numeric'
      placeholder="Insert width!"
    />
    <TextInput
      style={{ 
    	height: 40, 
    	borderColor: 'gray', 
      borderWidth: 1,
      marginBottom:10
    }}
      placeholderTextColor = 'black'
      onChangeText={text =>{ 
              if(text!=''){
                  setHeight(parseFloat(text));            
            }
            else
              setHeight(0);
            }          
      }
      value = {getHeight+""}
      keyboardType = 'numeric'
      placeholder="Insert height"
    />
          <Button
        title="+"
        onPress={() => {
          setCount(count+1)
          /* 1. Navigate to the Details route with params */
          if(num1!='' && num2!=''){
          navigation.navigate('Details', {
            itemId: 1,
            otherParam:sum,
          });
          setNum1('');
          setNum2('');
        }
        else 
        {
          //alert("Please fill all the text input");
        }
      }
      }
      />
    </View>
  );
}

function DetailsScreen({ route, navigation }) {
  /* 2. Get the param */
  const { itemId } = route.params;
  const { otherParam } = route.params;
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}>
      <Text>Details Screen</Text>
      <Text>{JSON.stringify(itemId)}</Text>
      <Text> {JSON.stringify(otherParam)}</Text>
      <Button title="Go to Home" onPress={() => {navigation.navigate('Home');}} />
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
}

const Stack = createStackNavigator();

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  tinyLogo: {
    width: 300,
    height: 300,
    marginBottom: 30
  },
  logo: {
    width: 66,
    height: 58,
  },
  button: {
    opacity:0.5,
    flexDirection: 'row', 
    height: 55.5, 
    backgroundColor: 'yellow',
    alignItems: 'center',
    justifyContent: 'center',
    elevation:0,
  }
});

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home"  options={{ title: 'My home',headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          }, }} component={HomeScreen}  />

        <Stack.Screen name="Details"options={{ 
            title: 'My Settings' ,
            headerStyle: {
              backgroundColor: '#f4511e',
            },
            headerTitleStyle:{
              color:'black'
            },
            headerTintColor: 'black',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
            headerRight: () => (
              // <Button
              //   onPress={() => alert('This is a button!')}
              //   title="Info"
              // />
              <View style={styles.parent}>
              <TouchableOpacity activeOpacity={0.95} style={styles.button} onPress={()=>alert('button')}>
                  <Text style={styles.text}>Button</Text>
              </TouchableOpacity>
          </View>
            ),
            headerTitle:"Screen2",
            }}  component={DetailsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
