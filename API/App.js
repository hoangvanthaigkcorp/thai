import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, Text, View, Image, StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview'
const App = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('http://demo1913415.mockable.io/getVideoHot', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }).then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  return (
    <View style={{ flex: 1, padding: 10,alignItems:'center',justifyContent:'center' }}>
      {isLoading ? <ActivityIndicator /> : (
        <FlatList
          horizontal={true}
          data={data}
          keyExtractor={({ id }, index) => id}
          renderItem={({ item }) => (
            <View
              style={styles.container}
            >
              <Image style={styles.Image} source={{ uri: item.avatar }} />
              <Text style={styles.Text}>{item.title} </Text>
            </View>
          )}
        />
      )}


      {/* webView */}
      {/* <WebView
        source={{
          uri: 'https://github.com/facebook/react-native'
        }}
        style={{ marginTop: 20 }}
      /> */}
    </View>
  );
};
const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  container: {
    width: 350,
    height: 280,
    backgroundColor: '#D3D3D3',
    borderRadius: 20,
    margin:10,
  },
  Image: {
    width: 350,
    height: 230,
    resizeMode: 'stretch',
    borderTopRightRadius:20,
    borderTopLeftRadius:20,
  },
  Text: {
    marginTop:5,
    marginLeft:20,
    marginRight:10,
    fontSize:15,
    fontWeight:'bold',
    textAlign:'auto',
  }
});
export default App;