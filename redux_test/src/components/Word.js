import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'


class Word extends Component {
    render() {
        const { en, vn } = this.props.myWord;
        return (
            <TouchableOpacity onPress={()=>{alert('a')}}>
                <View style={styles.container}>
                    <Text>{en}</Text>
                    <Text>{vn}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#D2DEF6',
        padding: 10,
        margin: 10,
    }

})
export default connect()(Word);