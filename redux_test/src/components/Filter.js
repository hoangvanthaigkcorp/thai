import React, { Component } from 'react';
import { View, TouchableOpacity, Text,StyleSheet } from 'react-native';
export default class Filter extends Component {
    render() {
        return (
            <View style={{ backgroundColor: '#583C3C', flex: 1, flexDirection: 'row' }}>
                <TouchableOpacity>
                    <Text>SHOW ALL</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Text>MEMORIZED</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Text>NEED PRACTICE</Text>
                </TouchableOpacity>
            </View>
        );
    }
}