import React, { Component } from 'react'
import { View, FlatList, TextInput } from 'react-native'
import { connect } from 'react-redux';
import Word from './Word'
import Filter from './Filter'

const Main = () => {

        return (
            <View style={{ backgroundColor: 'white', flex: 1, alignSelf: 'stretch' }}>
                <View style={{ flex: 10 }}>
                    {/* <FlatList
                        data={this.props.myWords}
                        renderItem={({ item }) => <Word myWord={item} />}
                        keyExtractor={item => item.id}
                    /> */}
                </View>
                {/* <Filter/> */}
                <TextInput placeholder='Nhap mau' onChange={()=>{dispatch()}}/>
            </View>
        )
}

function mapStateToProps(state) {
    return {
        myFilter: state.filterStatus,
        myWords: state.arrWords
    };
}

export default connect(mapStateToProps)(Main);